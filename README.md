A cherryPy simple app using the Redis server (you should have it running) to publish messages and subscribe to them. On the home page are shown all the topics and messages which are currently subscribed to.

Get running:
After installing the list of requirements, run the app like so:
python app.py

For running tests, run each test file separately (currently there's a problem with integration tests when run with the others), like so:
pytest -s utils_tests.py
