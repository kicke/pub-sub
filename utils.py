import redis


redis_cl = redis.StrictRedis


def get_redis_connection():
    """Make the Redis connection."""
    return redis_cl(
        host='localhost',
        port=6379,
        db=0
    )


def flush():
    """Clear redis of data."""
    redis = get_redis_connection()
    pubsub = redis.pubsub()
    pubsub.unsubscribe()
    redis.flushdb()


def publish(topic, content):
    """Publish a message on a topic."""
    redis = get_redis_connection()
    redis.publish(topic, content)
    save_data(redis, topic, content)


def save_data(redis, topic, content):
    """
    Save this topic to list, if there is a subscription to it.
    """
    subscribed_topics = redis.pubsub_channels()
    if topic.encode() in subscribed_topics:
        redis.lpush('topics', topic)

    # add this message to history for this topic
    redis.lpush(topic, content)


def subscribe(topic):
    """Subscribe to a topic."""
    redis = get_redis_connection()
    pubsub = redis.pubsub()
    pubsub.subscribe(topic)
    return pubsub


def saved_data():
    """Get previous messages."""
    redis = get_redis_connection()

    topics = redis.lrange('topics', 0, -1)
    dct = {}
    for topic in topics:
        contents = redis.lrange(topic, 0, -1)
        dct[topic] = contents

    return dct
