"""
Tests for the messaging system

Messages can be published to topics. Subscribers then receive message, if it's their topics.
"""
import utils


def test_publish_simple():
    utils.flush()
    topic = 'hello'
    content = 'world'

    utils.publish(topic, '')
    pubsub = utils.subscribe(topic)
    pubsub.get_message(ignore_subscribe_messages=True, timeout=0.3)

    utils.publish(topic, content)
    first_message = pubsub.get_message(timeout=0.3)
    data = first_message['data']
    data = data.decode('utf-8')

    assert data == content


def test_publish_before_subscribe__receives_no_messages():
    utils.flush()
    topic = 'hello'
    content = 'world'

    utils.publish(topic, '')
    utils.publish(topic, content)

    pubsub = utils.subscribe(topic)
    pubsub.get_message(ignore_subscribe_messages=True, timeout=0.3)
    first_message = pubsub.get_message(timeout=0.3)

    assert first_message is None


def test_publishing_to_different_topic__receives_no_messages():
    utils.flush()
    topic_1 = 'city'
    content_1 = 'street'
    topic_2 = 'village'
    content_2 = 'field'

    utils.publish(topic_1, '')
    utils.publish(topic_2, '')

    pubsub = utils.subscribe(topic_1)
    pubsub.get_message(ignore_subscribe_messages=True, timeout=0.3)

    utils.publish(topic_2, 'tractor')
    new_message = pubsub.get_message(timeout=0.3)

    assert new_message is None


def test_publish_simple__two_suscribers():
    utils.flush()
    topic = 'school'
    content = 'summer'

    utils.publish(topic, '')
    pubsub_1 = utils.subscribe(topic)
    pubsub_2 = utils.subscribe(topic)

    pubsub_1.get_message(ignore_subscribe_messages=True, timeout=0.3)
    pubsub_2.get_message(ignore_subscribe_messages=True, timeout=0.3)

    utils.publish(topic, content)

    m_1 = pubsub_1.get_message(timeout=0.3)
    data_1 = m_1['data'].decode('utf-8')
    m_2 = pubsub_2.get_message(timeout=0.3)
    data_2 = m_2['data'].decode('utf-8')

    assert data_1 == data_2 == content


def test_subscribed_to_wildcard__receives_messages():
    pass


def test_subscribed_to_wildcard__in_middle_receives_message():
    pass


def test_subscribed_with_multiple_wildcards__recieves_message():
    pass


def test_subscribed_to_wildcard__with_pound_receives_message():
    pass


def test_subscribed_to_wildcard__with_pound_and_plus_receives_message():
    pass


def test_publish_to_invalid_topic__fails():
    pass


def test_subscribe_to_empty_topic__fails():
    pass
