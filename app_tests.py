"""Test site."""
import cherrypy

from app import PubSub
from cptestcase import BaseCherryPyTestCase


def setUpModule():
    cherrypy.config.update({'environment': 'test_suite'})
    cherrypy.server.unsubscribe()

    cherrypy.tree.mount(PubSub(), '/')
    cherrypy.engine.start()

setup_module = setUpModule


def tearDownModule():
    cherrypy.engine.exit()

teardown_module = tearDownModule


class TestSite(BaseCherryPyTestCase):
    def test_publish(self):
        response = self.request('/publish')
        self.assertEqual(response.output_status, b'200 OK')


    def test_subscribe(self):
        response = self.request('/subscribe')
        self.assertEqual(response.output_status, b'200 OK')


    def test_publish_post(self):
        response = self.request('/publish_post', method='POST', topic='city')
        self.assertEqual(response.output_status, b'303 See Other')


    def test_subscribe_post(self):
        response = self.request('/subscribe_post', method='POST', topic='city')
        self.assertEqual(response.output_status, b'303 See Other')


if __name__ == '__main__':
    import unittest
    unittest.main()
