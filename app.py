import cherrypy
from jinja2 import Environment, FileSystemLoader
import os

import utils


env = Environment(loader=FileSystemLoader('templates'))


class PubSub(object):
    @cherrypy.expose
    def index(self):
        saved_data = utils.saved_data()
        args = {'saved_data': saved_data}
        template = env.get_template('index.html')
        return template.render(args)


    @cherrypy.expose
    def publish(self):
        template = env.get_template('publish.html')
        return template.render()


    @cherrypy.expose
    def subscribe(self):
        template = env.get_template('subscribe.html')
        return template.render()


    @cherrypy.expose
    def publish_post(self, topic=None, content=None):
        utils.publish(topic, content)
        raise cherrypy.HTTPRedirect('/')


    @cherrypy.expose
    def subscribe_post(self, topic=None):
        utils.subscribe(topic)
        raise cherrypy.HTTPRedirect('/')


site_config = os.path.join(os.path.dirname(__file__), 'site.conf')


if __name__ == '__main__':
    cherrypy.quickstart(PubSub(), config=site_config)
