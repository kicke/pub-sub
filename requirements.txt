cheroot==5.8.3
CherryPy==11.0.0
Jinja2==2.9.6
MarkupSafe==1.0
portend==2.1.2
py==1.4.34
pytest==3.2.1
pytz==2017.2
redis==2.10.6
six==1.10.0
tempora==1.9
