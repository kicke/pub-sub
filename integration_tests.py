import cherrypy

from cherrypy.test import helper
from app import PubSub


class SimpleCPTest(helper.CPWebCase):
    def setup_server():
        PubSub()
        cherrypy.tree.mount(PubSub())
    setup_server = staticmethod(setup_server)


    def test_publish_get(self):
        self.getPage('/publish')
        self.assertStatus('200 OK')
        self.assertHeader('Content-Type', 'text/html;charset=utf-8')
